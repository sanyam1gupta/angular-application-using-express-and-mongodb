#Angular Application using Express and MongoDB

This Angular Application has been built as part of Coursera's, Server Side Development with NodeJS,Express and MongoDB,course which is part of its
Full Stack Web and Multiplatform App Development specialization.

##Requirements-
1.NodeJS
2.Angular CLI 6.2.1
3.Express(for server-side development)
4.MongoDB